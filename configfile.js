var jsonconfigfile;
var confpages = [];
var rtbuf;
var confitems = {};
var rtitems = {};

function getSizeFromType(type) {
  switch(type) {
    case 0:
    case 8:
      return 1;
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
    case 5:
    case 9:
    case 10:
      return 4;
    case 4:
    case 6:
      return 8;
  }
  return 1;
}

function getAtOffset (view, offset, type, sign) {
  if(sign) {
    switch(type) {
      case 1:
        return view.getInt8(offset);
      case 2:
        return view.getInt16(offset, true);
      case 3:
        return view.getInt32(offset, true);
      case 4:
        return view.getInt64(offset, true);
      default:
        console.log('Invalid signed type');
    }
  } else {
    switch(type) {
      case 0:
        break;
      case 1:
      case 8:
        return view.getUint8(offset);
      case 2:
        return view.getUint16(offset, true);
      case 3:
      case 9:
      case 10:
        return view.getUint32(offset, true);
      case 4:
        return view.getUint64(offset, true);
      case 5:
        return view.getFloat32(offset, true);
      case 6:
        return view.getFloat64(offset, true);
      default:
        console.log('Invalid unsigned type');
    }
  }
}

var configfile = {
  parse:
    function (jsonstr) {
      jsonconfigfile = JSON.parse(jsonstr);
      _.forEach(jsonconfigfile.definition.confdef, function (pageItem) {
        var npage = pageItem.page;
        confpages[npage] = new ArrayBuffer(pageItem.size);
        _.forEach(pageItem.data, function (confItem) {
          if(confItem.id) {
            confitems[confItem.id] = confItem;
            confitems[confItem.id].page = npage;
          }
        });
      });
      console.log('Got ' + confpages.length + ' pages, page 0 is ' + confpages[0].byteLength + ' bytes');
      _.forEach(jsonconfigfile.definition.datastream.variables, function (rtvar) {
        if(rtvar.id) {
          rtitems[rtvar.id] = rtvar;
        }
      });
    },
  citemgetfromparent:
    function (address) {
      var arr = address.split('.');
      var parentid = arr[0];
      var parentoffset = arr[1];
      var parentend = arr[2];
      var val = configfile.citemgetraw(parentid);
      if(parentoffset !== undefined) {
        val = val >> parentoffset;
        var bitlength;
        if(parentend !== undefined) {
          var bitlength = parentend - parentoffset + 1;
        } else {
          bitlength = 1;
        }
        var mask = 0xFFFFFFFF >> (32 - bitlength);
        return val & mask;
      } else {
        return val;
      }
    },
  citemgetraw:
    function (id) {
      if(confitems[id]) {
        var item = confitems[id];
        var page = new DataView(confpages[item.page]);
        if(_.isString(item.address)) {
          return configfile.citemgetfromparent(item.address);
        }
        return getAtOffset(page, item.address, item.type, item.sign);
      }
    },
  citemget:
    function (id) {
      if(confitems[id]) {
        var item = confitems[id];
        var page = new DataView(confpages[item.page]);
        var val;
        if(_.isString(item.address)) {
          val = configfile.citemgetfromparent(item.address);
        } else {
          val = getAtOffset(page, item.address, item.type, item.sign);
        }
        if(item.offset) val += item.offset;
        if(item.inverse) val = 1 / val;
        if(item.scale) val *= item.scale;
        return val;
      }
    },
  rtgetfromparent:
    function (address) {
      var arr = address.split('.');
      var parentid = arr[0];
      var parentoffset = arr[1];
      var parentend = arr[2];
      var val = configfile.rtgetraw(parentid);
      if(parentoffset !== undefined) {
        val = val >> parentoffset;
        var bitlength;
        if(parentend !== undefined) {
          var bitlength = parentend - parentoffset + 1;
        } else {
          bitlength = 1;
        }
        var mask = 0xFFFFFFFF >> (32 - bitlength);
        return val & mask;
      } else {
        return val;
      }
    },
  rtgetraw:
    function (id) {
      if(rtitems[id]) {
        var item = rtitems[id];
        var view = new DataView(rtbuf);
        if(_.isString(item.address)) {
          val = configfile.rtgetfromparent(item.address);
        } else {
          val = getAtOffset(view, item.address, item.type, item.sign);
        }
        return val;
      }
    },
  rtget:
    function (id) {
      if(rtitems[id]) {
        var item = rtitems[id];
        var view = new DataView(rtbuf);
        if(_.isString(item.address)) {
          val = configfile.rtgetfromparent(item.address);
        } else {
          val = getAtOffset(view, item.address, item.type, item.sign);
        }
        if(item.offset) val += item.offset;
        if(item.inverse) val = 1 / val;
        if(item.scale) val *= item.scale;
        return val;
      }
    },
  rtgetString:
    function (id) {
      if(rtitems[id]) {
        var item = rtitems[id];
        var val = configfile.rtget(id);
        var digits = 0;
        if(item.digits !== undefined) {
          digits = item.digits;
        }
        val = val.toFixed(item.digits);
        if(item.unit) {
          var utfunit = new String(item.unit, "ISO-8859-1");
          val += ' ' + utfunit;
        }
        return val;
      }
    }
};
