var OutBuf = new ArrayBuffer(60);
var OutReport = new DataView(OutBuf);
var ConnectionID;
var OutReportSz;

var opqueue = [];

function memcpy(dst, dstOffset, src, srcOffset, length) {
  var dstU8 = new Uint8Array(dst, dstOffset, length);
  var srcU8 = new Uint8Array(src, srcOffset, length);
  dstU8.set(srcU8);
}

function cpy32(dst, dstOffset, src, srcOffset, length) {
  var dstU32 = new Uint8Array(dst, dstOffset, length);
  var srcU32 = new Uint8Array(src, srcOffset, length);
  dstU32.set(srcU32);
}

var usbhidcomms = {
  init: 
    function (cb) {
      chrome.hid.getDevices({
        filters: []
      }, function(arg) {
        if(arg.length) {
          if(arg[0].deviceId !== undefined) {
            console.log(JSON.stringify(arg[0]));
            OutReportSz = arg[0].maxOutputReportSize;
            chrome.hid.connect(arg[0].deviceId, function(connection) {
              if(chrome.runtime.lastError) {
                return cb(chrome.runtime.lastError.message);
              }
              ConnectionID = connection.connectionId;
              return usbhidcomms.GetVersion(cb);
            });
          }
        }
      });
    },
  GetVersion:
    function (cb) {
      OutReport.setUint16(0, 0x4953, true); // SI
      chrome.hid.send(ConnectionID, 0, OutBuf, function() {
        chrome.hid.receive(ConnectionID, function(rid, data) {
          console.log('Received ' + data.byteLength + ' bytes from controller');
          var InReport = new DataView(data);
          var signature = '';
          var i;
          for(i = 0; i < (data.byteLength - 10); i++) {
            var chr = InReport.getUint8(10+i);
            if(chr == 0) {
              i = data.byteLength;
            } else if(chr == 32) {
              signature += ' ';
            } else {
              signature += String.fromCharCode(chr);
            }
          }
          var controllerInfo = {
            model: String.fromCharCode(InReport.getUint8(2)) + String.fromCharCode(InReport.getUint8(3)),
          serialno: InReport.getUint16(4, true),
          fwversion: InReport.getUint16(6, true),
          defversion: InReport.getUint16(8, true),
          signature: signature
          };
          return cb(null, controllerInfo);
        });
      });

    },
  GetControllerData: 
    function (page, cb, offset) {
      if(ConnectionID === undefined) {
        if(_.isFunction(cb)) {
          return cb('No connection');
        } else {
          return;
        }
      }
      var datalength = OutReportSz - 7;
      datalength -= datalength % 4;
      if(offset === undefined) offset = 0;
      OutReport.setUint16(0, 0x5052, true); // RP
      OutReport.setUint8(2, page);
      OutReport.setUint16(3, offset, true);
      OutReport.setUint8(5, datalength);
      chrome.hid.send(ConnectionID, 0, OutBuf, function() {
        chrome.hid.receive(ConnectionID, function(rid, data) {
          var InReport = new DataView(data);
          var replyfrom = InReport.getUint16(0, true);
          if(replyfrom !== 0x5052) {
            if(_.isFunction(cb)) {
              return cb('Reply from wrong endpoint, got ' + replyfrom);
            } else {
              return;
            }
          }
          var returncode = InReport.getUint8(6);
          if(returncode !== 1) {
            if(_.isFunction(cb)) {
              return cb('Received return code ' + returncode + ' from controller');
            } else {
              return;
            }
          }
          var rxoffset = InReport.getUint16(3, true);
          if(rxoffset !== offset) {
            if(_.isFunction(cb)) {
              return cb('Received data from the wrong address');
            } else {
              return;
            }
          }
          var rxbytes = data.byteLength - 7;
          rxbytes = rxbytes - (rxbytes % 4);
//          console.log('Received ' + data.byteLength + ' bytes from controller, thereof ' + rxbytes + ' data');
          var bytesLeft = confpages[page].byteLength - offset;
          memcpy(confpages[page], offset, data, 7, bytesLeft > rxbytes ? rxbytes : bytesLeft);
          offset += rxbytes;
          if(offset < confpages[page].byteLength) {
            return usbhidcomms.GetControllerData(page, cb, offset);
          } else if(_.isFunction(cb)) {
            cb(null);
          }
        });
      });
    },
  GetRT:
    function (cb, offset, tmpbuf) {
      if(ConnectionID === undefined) {
        if(_.isFunction(cb)) {
          return cb('No connection');
        } else {
          return;
        }
      }
      var datalength = OutReportSz - 3;
      if(offset === undefined) offset = 0;
      OutReport.setUint16(0, 0x5452, true); // RT
      OutReport.setUint8(2, offset);
      chrome.hid.send(ConnectionID, 0, OutBuf, function() {
        chrome.hid.receive(ConnectionID, function(rid, data) {
          var InReport = new DataView(data);
          var replyfrom = InReport.getUint16(0, true);
          var rxoffset = InReport.getUint8(2);
          if(replyfrom !== 0x5452) {
            if(_.isFunction(cb)) {
              return cb('Reply from wrong endpoint, got ' + replyfrom);
            } else {
              return;
            }
          }
          if(rxoffset !== offset) {
            if(_.isFunction(cb)) {
              return cb('Reply at wrong offset, got ' + rxoffset);
            } else {
              return;
            }
          }
          var rxbytes = data.byteLength - ((offset === 0) ? 5 : 3);
          var totalbytes;
          if(offset === 0) {
            totalbytes = InReport.getUint16(3, true);
            tmpbuf = new ArrayBuffer(totalbytes);
          } else {
            totalbytes = tmpbuf.byteLength;
          }
//          console.log('Received ' + data.byteLength + ' bytes from controller, thereof ' + rxbytes + ' data total ' + totalbytes);
          var bytesLeft = totalbytes - (offset === 0 ? 0 : (offset * rxbytes) - 2);
          memcpy(tmpbuf, offset === 0 ? 0 : (offset * rxbytes) - 2, data, offset === 0 ? 5 : 3, bytesLeft > rxbytes ? rxbytes : bytesLeft);
          offset++;
          if(((offset * rxbytes) - 2) < totalbytes) {
            return usbhidcomms.GetRT(cb, offset, tmpbuf);
          } else {
            rtbuf = tmpbuf;
            if(_.isFunction(cb)) cb(null);
          }
        });
      });
    }
};
