var output = document.getElementById('output');
var OutBuf = new ArrayBuffer(60);
var OutReport = new DataView(OutBuf);
var comm;
var dashes = {};
var selecteddash = null;

chrome.storage.local.get('lastopenconfig', function (items) {
  if(items.lastopenconfig) {
    chrome.fileSystem.restoreEntry(items.lastopenconfig, function (fileEntry) {
      if (!fileEntry) {
        return;
      }
      chrome.fileSystem.getDisplayPath(fileEntry, function (dpath) {
        if(!dpath) return;
        document.getElementById('openlastconfig').value = 'Open ' + dpath;
      });
    });
  } else {
    document.getElementById('openlastconfig').disabled = true;
  }
});

document.getElementById('startlive').onclick = function() {
  livedisplay();
};

document.getElementById('openconfig').onclick = function() {
  console.log('lol dongs');
  chrome.fileSystem.chooseEntry({
    type: 'openFile', accepts:[{
            extensions: ['cud']
          }] 
  }, 
  function(fileEntry) {
    if (!fileEntry) {
      return;
    }
    fileEntry.file(function(file) {
      var reader = new FileReader();
      reader.onload = function(e) {
        chrome.storage.local.set(
          {lastopenconfig: chrome.fileSystem.retainEntry(fileEntry)});
        configfile.parse(e.target.result);
      };
      reader.readAsText(file);
    });
  });
};

document.getElementById('openlastconfig').onclick = function () {
  chrome.storage.local.get('lastopenconfig', function (items) {
    chrome.fileSystem.restoreEntry(items.lastopenconfig, function (fileEntry) {
      if (!fileEntry) {
        return;
      }
      fileEntry.file(function(file) {
        var reader = new FileReader();
        reader.onload = function(e) {
          configfile.parse(e.target.result);
          drawtabs(jsonconfigfile.dashes);
        };
        reader.readAsText(file);
      });
    });
  });
};

document.getElementById('startusb').onclick = function() {
  confpages[0] = new ArrayBuffer(16384);
  console.log('Starting USB comms');
  usbhidcomms.init(function(err, ControllerInfo) {
    if(err) {
      output.innerHTML = err;
    } else {
      output.innerHTML = JSON.stringify(ControllerInfo,null,2);
      comm = usbhidcomms;
      comm.GetControllerData(0, function(err) {
        if(err) {
          console.log('Error downloading config: ' + err);
        } else {
          console.log('Successfully downloaded config');
          comm.GetRT(function(err) {
            if(err) {
              console.log('Error getting RT: ' + err);
            } else {
              console.log('Successfully fetched RT');
              _.forEach(rtitems, function (item, id) {
                console.log('RT value "'+ id + '": ' + configfile.rtgetString(id));
              });
            }
          });
        }

      });
    }
  });
};

function drawtabs(descr) {
  var ohtml = '<table width="100%" border=2><tr>';
  _.forEach(descr, function(item) {
    dashes[item.id] = item;
    ohtml += '<td id="tab' + item.id + '">'+item.name+'</td>';
  });
  ohtml += '</tr></table>';
  document.getElementById('tabs').innerHTML = ohtml;
  _.forEach(descr, function(item) {
    document.getElementById('tab' + item.id).onclick = function() {
      selectdash(item.id);
    };
  });
}

function selectdash(id) {
  selecteddash = dashes[id];
  var htmlcode = '';
  _.forEach(selecteddash.widgets, function(widget) {
    // Todo: vantar staðsetningu
    htmlcode += '<div>' + widget.id + ' value: <span name="value.'+id+'.'+widget.id+'"></span></div>';
  });
  output.innerHTML = htmlcode;
  _.forEach(selecteddash.widgets, function(widget) {
    _.forEach(document.getElementsByName('value.' + id + '.' + widget.id), function(ele) {
      ele.innerHTML = configfile.rtgetString(widget.id);
    });
  });
}

function redrawdash() {
  if(selecteddash === null) return;
  _.forEach(selecteddash.widgets, function(widget) {
    _.forEach(document.getElementsByName('value.' + selecteddash.id + '.' + widget.id), function(ele) {
      ele.innerHTML = configfile.rtgetString(widget.id);
    });
  });  
}

function livedisplay() {
  comm.GetRT(function(err) {
    if(err) {
      console.log('Error getting RT: ' + err);
    } else {
      redrawdash();
      setTimeout(livedisplay, 50);
    }
  });
}
